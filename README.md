Kaji 0.2 - Monitoring tool set
==============================

Description
-----------

From (http://kaji-project.github.io/):

>Roughly, Kaji is juste a bundle of monitoring packages.
>
>The main goal of Kaji is to provide a simple and complete monitoring solution based on Shinken and Adagios.

The tools that it contains are:
* Shinken (2.0.3)
* Adagios (1.6.1)
* Pynag (0.9.1)
* Nagvis (1.7.10)
* Grafana (1.8.1)
* InfluxDB (lastest)

It expose 3 ports: 80 for Apache 2, 8086 for InfluxDB and finally, 22 for SSH access.

Usage
-----

    docker run --name kaji -d sebcworks/kaji:0.2
    docker logs kaji
    
    # Don't forget to change the default password ("tobechanged") of kaji user
    For example:
    docker exec -it kaji bash
    root@<CONTAINER_ID>:/# passwd kaji


Then you just have to follow the instructions that are printed when launching the container.
> http://<CONTAINER_IP>/


