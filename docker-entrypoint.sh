#!/bin/bash
set -e

if [ "$1" = 'welcome' ]; then
    cat >&2 <<EOF
################################################

The Kaji monitoring set should be now installed.
The 1st monitores host is localhost.

You can check now here: 
http://`grep $(hostname) /etc/hosts | cut -f1`/
You may also connect through SSH on the same IP
with the user "kaji".

DON'T FORGET TO CHANGE THE DEFAULT PASSWORD!

################################################

If you lauched the container in interactive
mode (i.e. with -it), you can press:
CTRL+P, CTRL+Q to detach yourself of it.

################################################
EOF

    exec /usr/bin/supervisord > /dev/null 2>&1
fi

# Not welcoming the user? Execute what he wants
exec "$@"