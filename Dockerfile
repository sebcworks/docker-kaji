# Dockerfile to install Kaji, the supervision tool.

FROM debian:wheezy

MAINTAINER Sebastien Collin <sebastien.collin@sebcworks.fr>

# -----------------------------------------------------

ENV DEBIAN_FRONTEND noninteractive

# Create kaji user, add it to sudoers and sets a default password to change later
RUN useradd -m -s /bin/bash -G sudo kaji && \
    echo kaji:tobechanged | chpasswd

# Create some needed directory
RUN mkdir -p /var/log/supervisor

# Installation of usefull tools: nano (text editor) and gosu (step-down from root, curl is also used to install gosu)
RUN gpg --keyserver pgp.mit.edu --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN apt-get update && \
    apt-get install -y curl \
       	    	       nano \
		       openssh-server \
       		       supervisor && \
    rm -rf /var/lib/apt/lists/* && \
    curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.2/gosu-$(dpkg --print-architecture)" && \
    curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.2/gosu-$(dpkg --print-architecture).asc" && \
    gpg --verify /usr/local/bin/gosu.asc && \
    rm /usr/local/bin/gosu.asc && \
    chmod +x /usr/local/bin/gosu


# Install kaji + dependencies with supervisor and SSH server
RUN apt-key adv --keyserver pgp.mit.edu --recv-keys 2320E8F8
# Wheezy backports for python-requests dependencie
RUN echo 'deb http://http.debian.net/debian wheezy-backports main' >> /etc/apt/sources.list.d/kaji.list
RUN echo 'deb http://deb.kaji-project.org/debian7/ amakuni main' >> /etc/apt/sources.list.d/kaji.list
RUN apt-get update && \
    apt-get -t wheezy-backports install -y python-requests && \
    apt-get install -y nagios-plugins && \
    apt-get install -y kaji

# Copy the configuration file for supervisor
COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf

# Copy the entry point script
COPY docker-entrypoint.sh /entrypoint.sh

# Launch the script that finish the installation
RUN bash /usr/sbin/kaji-finish-install

# -----------------------------------------------------

# Expore SSH, Apache and InfluxDB port
EXPOSE 80
EXPOSE 8086
EXPOSE 22

ENTRYPOINT ["/entrypoint.sh"]
# Show the welcome message (see entrypoint.sh) and start supervisor in foreground
CMD ["welcome"]